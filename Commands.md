# Commands

	git checkout -b [feature/branchname]		new Branch
	git checkout					Branch hopping
	git pull					Von Gitlab downloaden
	git merge [branchname]				override current branch with content of $branchname

# Step by Step feature branch erstellen:

- 1.Auf master branch einmal :git pull: machen damit der locale master branch auf dem neusten Stand ist
- 2.:git checkout -b "feature/$branchname": Um neuen branch zu erstellen (Copy vom master branch)
- - 2.1 Zum pruefen kann :git merge master: ausgefuehrt werden!
- 3.:git push: um branch zu veroeffentlichen!

# Step by Step commiten

- 1. Nach aenderung erst local testen
- 2. Zum staging branch wechseln :git checkout staging:
- 3. Feature branch auf staging holen :git merge feature/$branchname:
- 4. Testen ob alles noch funktioniert
- 5. Zum master branch wechseln :git checkout master:
- 6. Localen master branch updaten :git pull:
- 7. FEATURE_BRANCH auf master holen NICHT staging! :git merge feature/$branchname:
- 8. Master branch Testen ob alles noch funktioniert