using System;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class IPAPIServer 
    {
        public static event EventHandler<ServerOnlineEventArgs> OnIpServerOnline;
        public static bool isOnline = false;


        /// <summary>
        /// The port where the server is listening
        /// </summary>
        public Int32 port = 4840;

        /// <summary>
        /// The IP Adress where the server is listening
        /// </summary>
        public IPAddress serverAdress = IPAddress.Parse("127.0.0.1");

        private TcpListener server = null;
        private Thread readThread;

        public IPAPIServer() 
        {
            server = new TcpListener(serverAdress, port);
        }

        public void Start()
        {
            readThread = new Thread(() =>
            {
                try
                {
                    server.Start();
                    APILog.Debug("IP API server is listening on: " + serverAdress.ToString() + ":" + port);
                    isOnline = true;
                    OnIpServerOnline?.Invoke(null, new ServerOnlineEventArgs(true));

                    while (true)
                    {
                        // Accept all incoming connections
                        TcpClient client = server.AcceptTcpClient();
                        // Handle the connection in a new Thread
                        new IPClientHandler(client).Start();
                    }
                } catch (Exception error) when (error is ThreadAbortException || error is SocketException )
                {
                    // Server stopped
                }
            });
            readThread.Start();
        }

        public void Stop()
        {
            isOnline = false;
            server.Stop();
            readThread.Abort();
        }
    }
}