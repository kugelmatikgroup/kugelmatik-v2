﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KugelmatikLibrary.API
{
    public class ServerOnlineEventArgs
    {
        public bool serverIsOnline { get; set; }

        public ServerOnlineEventArgs(bool serverIsOnline)
        {
            this.serverIsOnline = serverIsOnline;
        }
    }
}
