﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KugelmatikLibrary.API
{
    public static class APILog
    {
        public static List<String> storage = new List<string>();
        public static event EventHandler<APILogFlushEventArgs> OnAPILogFlushBuffer;
        public static int bufferCapacity = 500;
        public static bool automaticFlush = false;

        private static StringBuilder buffer = new StringBuilder();


        public static void Write(LogLevel level, string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            string[] messageLines = message.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in messageLines)
            {
                string formattedMessage = string.Format("[{0}] {1} {2}", DateTime.Now.ToLongTimeString(), ("(" + level.ToString() + ")").PadRight(10), line);
                buffer.Append(formattedMessage);
                buffer.AppendLine();
                storage.Add(formattedMessage);
            }
            if (automaticFlush)
                FlushBuffer();

            else if (buffer.Length >= bufferCapacity)
                FlushBuffer();
        }

        public static void FlushBuffer()
        {
            string bufferedString = buffer.ToString();
            OnAPILogFlushBuffer?.Invoke(null, new APILogFlushEventArgs(bufferedString));
            buffer.Clear();
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Verbose, message) auf.
        /// </summary>
        /// <param name="message">Die Nachricht</param>
        public static void Verbose(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Write(LogLevel.Verbose, message);
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Debug, message) auf.
        /// </summary>
        /// <param name="message">Die Nachricht</param>
        public static void Debug(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Write(LogLevel.Debug, message);
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Info, message) auf.
        /// </summary>
        /// <param name="message">Die Nachricht</param>
        public static void Info(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Write(LogLevel.Info, message);
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Warning, message) auf.
        /// </summary>
        /// <param name="message">Die Nachricht</param>
        public static void Warning(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Write(LogLevel.Warning, message);
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Error, e.Message) und
        /// APILog.Write(LogLevel.Error, e.StackTrace) auf.
        /// </summary>
        /// <param name="e">Der Fehler</param>
        public static void Error(Exception e)
        {
            if (e == null)
                throw new ArgumentNullException("e");

            Error(e.Message);
            Error(e.StackTrace);
        }

        /// <summary>
        /// Ruft APILog.Write(LogLevel.Error, message) auf.
        /// </summary>
        /// <param name="message">Die Nachricht</param>
        public static void Error(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Write(LogLevel.Error, message);
        }
    }
}
