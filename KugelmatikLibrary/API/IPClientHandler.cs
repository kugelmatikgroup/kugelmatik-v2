﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class IPClientHandler : ClientHandler
    {
        private TcpClient client;
        private Thread thread;

        public IPClientHandler(TcpClient client)
        {
            this.client = client;
            stream = client.GetStream();
        }

        /// <summary>
        /// Starts a new Thread where the Client data is read and handled.
        /// </summary>
        public void Start()
        {
            APILog.Debug("API: Start handling IP client...");
            clientData = new ClientData(client.Client.RemoteEndPoint.ToString(), ConnectionType.IP);
            APIData.AddConnectedClient(this);
            thread = new Thread(ReadDataAsync);
            thread.Start();
        }

        private async void ReadDataAsync()
        {
            try
            {
                var bytesRead = 0;
                string cachedString = "";
                while ((bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string chunk = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                    cachedString += chunk;

                    if (cachedString.Contains("<EOP>"))
                    {
                        cachedString = cachedString.Replace("<EOP>", "");
                        APIHandler.HandleString(cachedString, this);
                        cachedString = "";
                    }
                }
                client.Close();
                APIData.RemoveConnectedClient(this);
                APILog.Debug("API: Client connection closed. ID: " + clientData.ID);
            } catch (Exception)
            {
                APIData.RemoveConnectedClient(this);
                APILog.Error("API: Client connection abort. ID: " + clientData.ID);
            }
        }
    }
}
