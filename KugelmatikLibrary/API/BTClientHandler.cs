﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using InTheHand.Net.Sockets;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class BTClientHandler : ClientHandler
    {
        private BluetoothClient client;
        private Thread thread;

        public BTClientHandler(BluetoothClient client)
        {
            this.client = client;
            stream = client.GetStream();
        }

        /// <summary>
        /// Starts a new Thread where the Client data is read and handled.
        /// </summary>
        public void Start()
        {
            APILog.Debug("API: Start handling BT client...");
            clientData = new ClientData(client.RemoteMachineName, ConnectionType.BT);
            APIData.AddConnectedClient(this);
            thread = new Thread(ReadDataAsync);
            thread.Start();
        }

        private async void ReadDataAsync()
        {
            try
            {
                var bytesRead = 0;
                string cachedString = "";
                while ((bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string chunk = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                    cachedString += chunk;

                    if (cachedString.Contains("<EOP>"))
                    {
                        string[] packetStrings = cachedString.Split(new[] { "<EOP>" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string packetString in packetStrings)
                        {
                            packetString.Replace("<EOP>", "");
                        }
                        APIHandler.HandleString(packetStrings[0], this);
                        if (packetStrings.Length > 1)
                        {
                            cachedString = packetStrings[1];
                        } else
                        {
                            cachedString = "";
                        }
                    }
                }
                client.Close();
                APIData.RemoveConnectedClient(this);
                APILog.Debug("API: Client connection closed. ID: " + clientData.ID);
            }
            catch (Exception)
            {
                APIData.RemoveConnectedClient(this);
                APILog.Error("API: Client connection abort. ID: " + clientData.ID);
            }
        }
    }
}