﻿namespace KugelmatikLibrary.API
{
    public class Coordinate
    {
        public int clusterPosX;
        public int clusterPosY;
        public int spherePosX;
        public int spherePosY;

        public Coordinate(int clusterPosX, int clusterPosY, int spherePosX, int spherePosY)
        {
            this.clusterPosX = clusterPosX;
            this.clusterPosY = clusterPosY;
            this.spherePosX = spherePosX;
            this.spherePosY = spherePosY;
        }

        public Coordinate()
        {

        }

        public override string ToString()
        {
            return clusterPosX + ", " + clusterPosY + ", " + spherePosX + ", " + spherePosY;
        }
    }
}
