﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public enum Command : int
    {
        UNDEFINED = -1,
        SEND_HOME = 0,
        SPHERE_DOWN = 1,
        SPHERE_UP = 2,
        STOP = 3,
        SET = 4,
        FIX = 5
    }

    public class NetworkPackageSection
    {
        public const string DATA_SETS = "dataSets";
        public const string CLIENT_INFO = "clientInfo";
    }

    public class DataSetSection
    {
        public const string COMMAND = "cmd";
        public const string COORDINATE = "coordinate";
        public const string HEIGHT_VALUE = "heightValue";
    }

    public class APIHandler
    {
        private static Kugelmatik kugelmatik;

        public static Command GetCommand(int number)
        {
            foreach (Command cmd in Enum.GetValues(typeof(Command)))
            {
                if ((int)cmd == number)
                {
                    return cmd;
                }
            }
            return Command.UNDEFINED;
        }

        /// <summary>
        /// Validate the JSON string and tries to execute it.
        /// Returns true if data is valid.
        /// Returns false if data is invalid.
        /// </summary>
        /// 
        public static void HandleString(string jsonString, ClientHandler handler)
        {
            new Thread(() =>
            {
                APIData.SetReceivedPackets(1);
                // 0 - 1 ms
                List<DataSet> dataSets = GetDataSetList(jsonString);
                if (dataSets != null)
                {
                    DateTime start = DateTime.Now;
                    // 80 - 100 ms
                    ExecuteDataSets(dataSets);
                    //Debug.WriteLine(dataSets.Count);
                    //Debug.WriteLine("Execute: " + (DateTime.Now - start).Milliseconds);
                    handler.Send("OK");
                    APIData.SetSuccessfulPackets(1);
                }
                else
                {
                    handler.Send("ERROR");
                    APIData.SetFailedPackets(1);
                }
            }).Start();
        }


        /// <summary>
        /// Converts a string to a list of DataSets
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns>A valid list of DataSets. null if the string cannot be converted</returns>
        public static List<DataSet> GetDataSetList(string jsonString)
        {
            Debug.WriteLine("Execute MSG: " + jsonString);
            JObject json;
            List<DataSet> dataSetList = new List<DataSet>();

            // Check if the incoming data is in a valid json format and convert it

            try
            {
                json = (JObject)JsonConvert.DeserializeObject(jsonString);
            }
            catch (Exception) 
            {
                APILog.Error("API: \"" + jsonString + "\" is not in a valid JSON format.");
                return null;
            }
            if (json == null)
            {
                return null;
            }

            // Check if all required sections are available

            if (!json.ContainsKey(NetworkPackageSection.DATA_SETS))
            {
                APILog.Error("API: Missing section '" + NetworkPackageSection.DATA_SETS + "' in package (See Docs)");
                return null;
            }
            if (!json.ContainsKey(NetworkPackageSection.CLIENT_INFO))
            {
                APILog.Error("API: Missing section '" + NetworkPackageSection.CLIENT_INFO + "' in package (See Docs)");
                return null;
            }

            // Check Datasets

            List<JObject> dataSetsJson = new List<JObject>();
            try
            {
                foreach (JToken dataSetToken in JArray.Parse(json[NetworkPackageSection.DATA_SETS].ToString()))
                {
                    dataSetsJson.Add((JObject)JsonConvert.DeserializeObject(dataSetToken.ToString()));
                }
            }
            catch (Exception)
            {
                APILog.Error("API: \"" + jsonString + "\" is not in a valid JSON format.");
                return null;
            }
            if (dataSetsJson == null)
            {
                return null;
            }

            foreach (JObject dataSetJson in dataSetsJson)
            {
                // Data is available?
                if (!dataSetJson.ContainsKey(DataSetSection.COMMAND)) {
                    APILog.Error("API: Missing section '" + DataSetSection.COMMAND + "' in package (See Docs)");
                    return null;
                }
                if (!dataSetJson.ContainsKey(DataSetSection.COORDINATE))
                {
                    APILog.Error("API: Missing section '" + DataSetSection.COORDINATE + "' in package (See Docs)");
                    return null;
                }
                if (!dataSetJson.ContainsKey(DataSetSection.HEIGHT_VALUE))
                {
                    APILog.Error("API: Missing section '" + DataSetSection.HEIGHT_VALUE + "' in package (See Docs)");
                    return null;
                }

                DataSet dataSet = new DataSet();


                // Check and get command
                try
                {
                    dataSet.cmd = GetCommand((int)dataSetJson[DataSetSection.COMMAND]);
                }
                catch (FormatException)
                {
                    APILog.Error("API: Wrong datatype in section: '" + DataSetSection.COMMAND + "'");
                    return null;
                }

                // Check and get coordinate
                try
                {
                    Coordinate cord = new Coordinate();
                    JArray jsonCoordinateList = JArray.Parse(dataSetJson[DataSetSection.COORDINATE].ToString());
                    cord.clusterPosX = (int)jsonCoordinateList[0];
                    cord.clusterPosY = (int)jsonCoordinateList[1];
                    cord.spherePosX = (int)jsonCoordinateList[2];
                    cord.spherePosY = (int)jsonCoordinateList[3];
                    dataSet.coordinate = cord;
                }
                catch (Exception e) when (e is JsonReaderException || e is FormatException)
                {
                    APILog.Error("API: Wrong datatype in section: '" + DataSetSection.COORDINATE + "' (see docs)");
                    return null;
                }
                catch (ArgumentOutOfRangeException)
                {
                    APILog.Error("API: Coordinates have to be 4 dimensional.");
                    return null;
                }

                // Check and get height
                try
                {
                    dataSet.height = (int)dataSetJson[DataSetSection.HEIGHT_VALUE];
                }
                catch (FormatException)
                {
                    APILog.Error("API: Wrong datatype in section: '" + DataSetSection.HEIGHT_VALUE + "' (see docs)");
                    return null;
                }
                dataSetList.Add(dataSet);
            }

            // If all data is valid return it
            return dataSetList;
        }

        private static void ExecuteDataSets(List<DataSet> dataSets)
        {
            foreach (DataSet dataSet in dataSets)
            {
                Coordinate cord = dataSet.coordinate;
                if (dataSet.cmd == Command.UNDEFINED)
                {
                    APILog.Warning("API: Command undefinied");
                    return;
                }
                else if (dataSet.cmd == Command.SET)
                {
                    try
                    {
                        // Move all
                        if (cord.clusterPosX < 0 && cord.clusterPosY < 0 && cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            foreach (Cluster cluster in kugelmatik.EnumerateClusters())
                            {
                                foreach (Stepper stepper in cluster.EnumerateSteppers())
                                {
                                    stepper.Set((ushort)dataSet.height);
                                }
                            }
                        }

                        // Move whole cluster
                        else if (cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            foreach (Stepper stepper in cluster.EnumerateSteppers())
                            {
                                stepper.Set((ushort)dataSet.height);
                            }
                        }
                        else
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            Stepper stepper = cluster.GetStepperByPosition(cord.spherePosX, cord.spherePosY);
                            stepper.Set((ushort)dataSet.height);
                        }
                    }

                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
                else if (dataSet.cmd == Command.SPHERE_UP)
                {
                    try
                    {
                        // Move all
                        // TODO extra cmd
                        if (cord.clusterPosX < 0 && cord.clusterPosY < 0 && cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            foreach (Cluster cluster in kugelmatik.EnumerateClusters())
                            {
                                foreach (Stepper stepper in cluster.EnumerateSteppers())
                                {
                                    stepper.Set((ushort)(stepper.Height - dataSet.height));
                                }
                            }
                        }
                        // Move whole cluster
                        else if (cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            foreach (Stepper stepper in cluster.EnumerateSteppers())
                            {
                                stepper.Set((ushort)(stepper.Height - dataSet.height));
                            }
                        }
                        else
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            Stepper stepper = cluster.GetStepperByPosition(cord.spherePosX, cord.spherePosY);
                            stepper.Set((ushort)(stepper.Height - dataSet.height));
                        }

                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
                else if (dataSet.cmd == Command.SPHERE_DOWN)
                {
                    try
                    {
                        // Move all
                        // TODO extra cmd
                        if (cord.clusterPosX < 0 && cord.clusterPosY < 0 && cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            foreach (Cluster cluster in kugelmatik.EnumerateClusters())
                            {
                                foreach (Stepper stepper in cluster.EnumerateSteppers())
                                {
                                    stepper.Set((ushort)(stepper.Height + dataSet.height));
                                }
                            }
                        }
                        // Move whole cluster
                        else if (cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            foreach (Stepper stepper in cluster.EnumerateSteppers())
                            {
                                stepper.Set((ushort)(stepper.Height + dataSet.height));
                            }
                        }
                        else
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            Stepper stepper = cluster.GetStepperByPosition(cord.spherePosX, cord.spherePosY);
                            stepper.Set((ushort)(stepper.Height + dataSet.height));
                        }
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
                else if (dataSet.cmd == Command.SEND_HOME)
                {
                    try
                    {
                        // Send all home
                        if (cord.clusterPosX < 0 && cord.clusterPosY < 0 && cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            foreach (Cluster cluster in kugelmatik.EnumerateClusters())
                            {
                                foreach (Stepper stepper in cluster.EnumerateSteppers())
                                {
                                    stepper.SendHome();
                                }
                            }
                        }

                        // Send whole cluster home
                        else if (cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            foreach (Stepper stepper in cluster.EnumerateSteppers())
                            {
                                stepper.SendHome();
                            }
                        }
                        // Send sphere home
                        else
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            Stepper stepper = cluster.GetStepperByPosition(cord.spherePosX, cord.spherePosY);
                            stepper.SendHome();
                        }

                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
                else if (dataSet.cmd == Command.FIX)
                {
                    try
                    {
                        // Fix all
                        if (cord.clusterPosX < 0 && cord.clusterPosY < 0 && cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            foreach (Cluster cluster in kugelmatik.EnumerateClusters())
                            {
                                foreach (Stepper stepper in cluster.EnumerateSteppers())
                                {
                                    stepper.SendFix();
                                }
                            }
                        }

                        // Fix whole cluster
                        else if (cord.spherePosX < 0 && cord.spherePosY < 0)
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            foreach (Stepper stepper in cluster.EnumerateSteppers())
                            {
                                stepper.SendFix();
                            }
                        }
                        // Fix one sphere
                        else
                        {
                            Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                            Stepper stepper = cluster.GetStepperByPosition(cord.spherePosX, cord.spherePosY);
                            stepper.SendFix();
                        }

                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
                else if (dataSet.cmd == Command.STOP)
                {
                    try
                    {
                        Cluster cluster = kugelmatik.GetClusterByPosition(cord.clusterPosX, cord.clusterPosY);
                        cluster.SendStop();
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Ignored
                    }
                }
            }
        }

        public static void SetKugelmatik(Kugelmatik kugelmatik)
        {
            APIHandler.kugelmatik = kugelmatik;
        }
    }
}
