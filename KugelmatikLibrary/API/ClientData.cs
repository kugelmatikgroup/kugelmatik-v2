﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class ClientData
    {
        /// <summary>
        /// The IP or UUID as string. The string will be used in the list of all connected clients.
        /// </summary>
        public string ID = "";

        /// <summary>
        /// The type of the connection. This data will be used in the list of all connected clients.
        /// </summary>
        public ConnectionType connectionType;

        public ClientData(string ID, ConnectionType connectionType)
        {
            this.ID = ID;
            this.connectionType = connectionType;
            APILog.Debug("Client recognized! ID: " + ID);
        }

    }
}
