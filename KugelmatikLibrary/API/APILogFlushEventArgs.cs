﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KugelmatikLibrary.API
{
    public class APILogFlushEventArgs : EventArgs
    {
        public string Buffer { get; private set; }

        public APILogFlushEventArgs(string buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));

            this.Buffer = buffer;
        }
    }
}
