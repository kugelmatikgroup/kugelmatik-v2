﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KugelmatikLibrary.API.Info
{
    public class UpdateStatsEventArgs : EventArgs
    {
        public StatsField updatedStatsField { get; set; }

        public UpdateStatsEventArgs(StatsField updatedStatsField)
        {
            this.updatedStatsField = updatedStatsField;
        }
    }
}
