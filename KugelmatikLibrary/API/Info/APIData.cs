﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Threading;

namespace KugelmatikLibrary.API.Info
{
    public class APIData
    {
        public static List<ClientHandler> connectedClients = new List<ClientHandler>();
        public static int receivedPackets = 0;
        public static int receivedPacketsPerMin = 0;
        public static int successfulPackets = 0;
        public static int failedPackets = 0;

        public static event EventHandler<UpdateStatsEventArgs> OnStatsUpdate;

        // Last minute data
        public static List<DateTime> receivedPacketsTimes = new List<DateTime>();

        public static void Write(String message)
        {
        }

        public static void ResetStats()
        {
            receivedPackets = 0;
            receivedPacketsPerMin = 0;
            successfulPackets = 0;
            failedPackets = 0;

            receivedPacketsTimes.Clear();

            UpdateAllStats();
        }

        private static void UpdateAllStats()
        {
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.CONNECTED_CLIENTS));
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.RECEIVED_PACKETS));
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.RECEIVED_PACKETS_MIN));
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.SUCCESSFUL_PACKETS));
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.FAILED_PACKETS));
        }

        public static void AddConnectedClient(ClientHandler handler)
        {
            connectedClients.Add(handler);
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.CONNECTED_CLIENTS));
        }

        public static void RemoveConnectedClient(ClientHandler handler)
        {
            connectedClients.Remove(handler);
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.CONNECTED_CLIENTS));
        }

        public static void SetReceivedPackets(int number)
        {
            if (receivedPackets + number > 0)
            {
                receivedPackets += number;
                receivedPacketsTimes.Add(DateTime.Now);
                OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.RECEIVED_PACKETS));
            }
        }

        public static void SetReceivedPacketsMin(int number)
        {
            receivedPacketsPerMin = number;
            OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.RECEIVED_PACKETS_MIN));
        }

        public static void SetSuccessfulPackets(int number)
        {
            if (successfulPackets + number > 0)
            {
                successfulPackets += number;
                OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.SUCCESSFUL_PACKETS));
            }
        }

        public static void SetFailedPackets(int number)
        {
             if (failedPackets + number > 0)
            {
                failedPackets += number;
                OnStatsUpdate?.Invoke(null, new UpdateStatsEventArgs(StatsField.FAILED_PACKETS));
            }
        }

        public static ClientHandler GetClientHandler(string ID)
        {
            foreach (ClientHandler handler in connectedClients)
            {
                if (ID.Equals(handler.clientData.ID))
                {
                    return handler;
                }
            }
            return null;
        }
    }
}
