﻿namespace KugelmatikLibrary.API.Info
{
    public enum StatsField
    {
        CONNECTED_CLIENTS,
        RECEIVED_PACKETS,
        RECEIVED_PACKETS_MIN,
        SUCCESSFUL_PACKETS,
        FAILED_PACKETS
    }
}
