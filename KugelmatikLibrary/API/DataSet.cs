﻿namespace KugelmatikLibrary.API
{
    public class DataSet
    {
        public Command cmd;
        public Coordinate coordinate;
        public int height;
        public Kugelmatik kugelmatik;

        public DataSet(Command cmd, Coordinate coordinate, int height)
        {
            this.cmd = cmd;
            this.coordinate = coordinate;
            this.height = height;      
        }

        public DataSet()
        {

        }
    }
}
