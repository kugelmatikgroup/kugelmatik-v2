﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using InTheHand.Net.Bluetooth;
using System.Diagnostics;
using InTheHand.Net.Sockets;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class BTAPIServer
    {
        public static event EventHandler<ServerOnlineEventArgs> OnBtServerOnline;
        public static bool isOnline = false;

        /// <summary>
        /// The IP Adress where the server is listening. Windows -> https://bluetoothinstaller.com/bluetooth-command-line-tools
        /// </summary>
        /// 0000110a-0000-1000-8000-00805f9b34fb, 0000111e-0000-1000-8000-00805f9b34fb, 00001115-0000-1000-8000-00805f9b34fb, 0000110e-0000-1000-8000-00805f9b34fb, 0000111f-0000-1000-8000-00805f9b34fb, c7f94713-891e-496a-a0e7-983a0946126e, e0cbf06c-cd8b-4647-bb8a-263b43f0f974
        public Guid serverGuid = Guid.Parse("{e0cbf06c-cd8b-4647-bb8a-263b43f0f974}");

        private BluetoothListener server = null;
        private Thread readThread;

        public BTAPIServer()
        {
            server = new BluetoothListener(serverGuid);
        }

        public void Start()
        {
            readThread = new Thread(() =>
            {
                try
                {
                    server.Start();
                    APILog.Debug("Bluetooth API server is listening on: " + serverGuid.ToString());
                    isOnline = true;
                    OnBtServerOnline?.Invoke(null, new ServerOnlineEventArgs(true));


                    while (true)
                    {
                        // Accept all incoming connections
                        BluetoothClient client = server.AcceptBluetoothClient();
                        // Handle the connection in a new Thread
                        new BTClientHandler(client).Start();
                    }
                }
                catch (ThreadAbortException)
                {
                    // Server stopped;
                } catch (SocketException)
                {
                    APILog.Error("API: Can't start Bluetooth API Server. Bluetooth disabled?");
                }
            });
            readThread.Start();
        }

        public void Stop()
        {
            isOnline = false;
            server.Stop();
            readThread.Abort();
        }
    }
}