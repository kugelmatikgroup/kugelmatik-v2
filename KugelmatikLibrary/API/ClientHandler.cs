﻿using System;
using System.Text;
using System.Net.Sockets;
using KugelmatikLibrary.API.Info;

namespace KugelmatikLibrary.API
{
    public class ClientHandler
    {
        public NetworkStream stream;
        public ClientData clientData;

        /// <summary>
        /// The buffer/ chunk used for the incoming stream.
        /// </summary>
        public byte[] buffer = new byte[4096];

        public void Send(String data)
        {
            try
            {
                if (stream != null && stream.CanWrite)
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(data);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                }
            } catch (System.IO.IOException)
            {

            }
        }

        public void Close()
        {
            stream.Close();
        }
    }
}
