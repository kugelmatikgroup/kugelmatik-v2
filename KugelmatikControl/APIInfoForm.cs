﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KugelmatikLibrary.API;
using KugelmatikLibrary.API.Info;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Defaults;
using System.Threading;

namespace KugelmatikControl
{
    public partial class APIInfoForm : Form
    {
        // Chart data
        private ChartValues<ObservablePoint> receivedPacketsValues = new ChartValues<ObservablePoint>();
        private Thread statsUpdaterThread;

        // Connected clients data
        private ClientHandler currentSelectedHandler;

        public APIInfoForm()
        {
            InitializeComponent();

            APIData.OnStatsUpdate += UpdateStats;

            InitServerData();
            InitAPILog();
            InitChart();
            UpdateAllStats();
        }

        private void InitServerData()
        {
            if (BTAPIServer.isOnline)
            {
                btServerOnlineLabel.Text = "✔️";
            }
            else
            {
                btServerOnlineLabel.Text = "❌";
            }
            if (IPAPIServer.isOnline)
            {
                ipServerOnlineLabel.Text = "✔️";
            }
            else
            {
                ipServerOnlineLabel.Text = "❌";
            }
        }

        private void InitAPILog()
        {
            APILog.OnAPILogFlushBuffer += OnAPILogFlushBuffer;
            APILog.automaticFlush = true;
            StringBuilder builder = new StringBuilder();
            foreach (String line in APILog.storage)
            {
                builder.Append(line);
            }
            apiLogTextBox.Text = builder.ToString();
            APILog.FlushBuffer();
        }

        private void InitChart()
        {
            statsChart.AxisY.Add(new Axis { MinValue = 0 });

            SeriesCollection series = new SeriesCollection();
            receivedPacketsValues.Add(new ObservablePoint(0, 0));
            series.Add(new LineSeries
            {
                Title = "Empfangene Pakete",
                Values = receivedPacketsValues,
                PointGeometrySize = 5
            });
            statsChart.Series = series;

            StartStatsChartUpdater();
        }

        private void UpdateAllStats()
        {
            UpdateStats(null, new UpdateStatsEventArgs(StatsField.CONNECTED_CLIENTS));
            UpdateStats(null, new UpdateStatsEventArgs(StatsField.RECEIVED_PACKETS));
            UpdateStats(null, new UpdateStatsEventArgs(StatsField.SUCCESSFUL_PACKETS));
            UpdateStats(null, new UpdateStatsEventArgs(StatsField.FAILED_PACKETS));
        }

        private void UpdateStats(object sender, UpdateStatsEventArgs args)
        {
            if (args.updatedStatsField == StatsField.CONNECTED_CLIENTS)
            {
                if (connectedClientsCount.InvokeRequired)
                {
                    connectedClientsCount.BeginInvoke(new EventHandler<UpdateStatsEventArgs>(UpdateStats), sender, args);
                } else
                {
                    connectedClientsCount.Text = APIData.connectedClients.Count.ToString();
                    connectedClientsList.Items.Clear();
                    foreach (ClientHandler handler in APIData.connectedClients)
                    {
                        ClientData data = handler.clientData;
                        string newItem = "ID: \n" + data.ID + "\n";
                        if (data.connectionType == ConnectionType.IP)
                        {
                            newItem += " | Connection type: IP";
                        } else if (data.connectionType == ConnectionType.BT)
                        {
                            newItem += " | Connection type: BT";
                        } else
                        {
                            newItem += " | Connection type: Unknown";
                        }
                        connectedClientsList.Items.Add(newItem);
                    }
                }
            } else if (args.updatedStatsField == StatsField.RECEIVED_PACKETS)
            {
                if (receivedPacketsCount.InvokeRequired)
                    receivedPacketsCount.BeginInvoke(new EventHandler<UpdateStatsEventArgs>(UpdateStats), sender, args);
                else
                    receivedPacketsCount.Text = APIData.receivedPackets.ToString();

            } else if (args.updatedStatsField == StatsField.RECEIVED_PACKETS_MIN)
            {
                if (receivedPacketsMinCount.InvokeRequired)
                    receivedPacketsMinCount.BeginInvoke(new EventHandler<UpdateStatsEventArgs>(UpdateStats), sender, args);
                else
                    receivedPacketsMinCount.Text = APIData.receivedPacketsPerMin.ToString();

            }
            else if (args.updatedStatsField == StatsField.SUCCESSFUL_PACKETS)
            {
                if (successfulPacketsCount.InvokeRequired)
                    successfulPacketsCount.BeginInvoke(new EventHandler<UpdateStatsEventArgs>(UpdateStats), sender, args);
                else
                    successfulPacketsCount.Text = APIData.successfulPackets.ToString();
            } else if (args.updatedStatsField == StatsField.FAILED_PACKETS)
            {
                if (failedPacketsCount.InvokeRequired)
                    failedPacketsCount.BeginInvoke(new EventHandler<UpdateStatsEventArgs>(UpdateStats), sender, args);
                else
                    failedPacketsCount.Text = APIData.failedPackets.ToString();
            }
        }

        private void OnAPILogFlushBuffer(object sender, APILogFlushEventArgs args)
        {
            if (apiLogTextBox.InvokeRequired)
                apiLogTextBox.BeginInvoke(new EventHandler<APILogFlushEventArgs>(OnAPILogFlushBuffer), sender, args);
            else
                apiLogTextBox.AppendText(args.Buffer);
        }

        private void statsResetButton_Click(object sender, EventArgs e)
        {
            APIData.ResetStats();
        }

        private void StartStatsChartUpdater()
        {
            StopStatsChartUpdater();
            statsUpdaterThread = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        APIData.receivedPacketsTimes.RemoveAll(time => DateTime.Now.Subtract(time).TotalSeconds > 60);
                        APIData.receivedPacketsPerMin = APIData.receivedPacketsTimes.Count;
                        receivedPacketsValues.Add(new ObservablePoint(receivedPacketsValues.Last().X + 1, APIData.receivedPacketsPerMin));
                        APIData.SetReceivedPacketsMin(APIData.receivedPacketsPerMin);
                        if (receivedPacketsValues.Count > 25)
                        {
                            receivedPacketsValues.RemoveAt(0);
                        }
                        Thread.Sleep(1000);
                    }
                } catch (ThreadAbortException)
                {
                    // Updater stopped - Ignored
                }
            });
            statsUpdaterThread.Start();
        }

        private void StopStatsChartUpdater()
        {
            if (statsUpdaterThread != null && statsUpdaterThread.IsAlive)
            {
                statsUpdaterThread.Abort();
            }
        }

        private void APIInfoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopStatsChartUpdater();
        }

        private void connectedClientsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (connectedClientsList.SelectedItem == null)
                return;

            ClientHandler handler;
            try
            {
                handler = APIData.connectedClients[connectedClientsList.SelectedIndex];
            } catch (IndexOutOfRangeException)
            {
                if (b_removeClient.Enabled)
                    b_removeClient.Enabled = false;
                return;
            }
            
            currentSelectedHandler = handler;
            b_removeClient.Enabled = true;
        }

        private void connectedClientsList_KeyDown(object sender, KeyEventArgs e)
        {
            if (currentSelectedHandler == null)
                return;
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.X)
            {
                currentSelectedHandler.Close();
                b_removeClient.Enabled = false;
            }
        }

        private void removeClientButton_Click(object sender, EventArgs e)
        {
            if (currentSelectedHandler == null)
                return;
            currentSelectedHandler.Close();
            b_removeClient.Enabled = false;
        }

        private void apiLogClearButton_Click(object sender, EventArgs e)
        {
            APILog.storage.Clear();
            apiLogTextBox.Clear();
        }

        private void b_removeAllClients_Click(object sender, EventArgs e)
        {
            if (APIData.connectedClients.Count > 0)
            {
                foreach (ClientHandler handler in APIData.connectedClients)
                {
                    handler.Close();
                }
                UpdateStats(null, new UpdateStatsEventArgs(StatsField.CONNECTED_CLIENTS));
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            APILog.OnAPILogFlushBuffer -= OnAPILogFlushBuffer;
            APILog.automaticFlush = false;
        }
    }
}
