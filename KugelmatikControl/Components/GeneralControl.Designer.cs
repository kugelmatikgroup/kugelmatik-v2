﻿
namespace KugelmatikControl
{
    partial class GeneralControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.allClusterSlider = new System.Windows.Forms.TrackBar();
            this.allClusterInput = new System.Windows.Forms.NumericUpDown();
            this.allClusterLabel = new System.Windows.Forms.Label();
            this.apiInfoButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.allClusterSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allClusterInput)).BeginInit();
            this.SuspendLayout();
            // 
            // allClusterSlider
            // 
            this.allClusterSlider.Location = new System.Drawing.Point(236, 0);
            this.allClusterSlider.Maximum = 8000;
            this.allClusterSlider.Name = "allClusterSlider";
            this.allClusterSlider.Size = new System.Drawing.Size(299, 45);
            this.allClusterSlider.TabIndex = 0;
            this.allClusterSlider.ValueChanged += new System.EventHandler(this.allClusterSlider_ValueChanged);
            // 
            // allClusterInput
            // 
            this.allClusterInput.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allClusterInput.Location = new System.Drawing.Point(145, 0);
            this.allClusterInput.Maximum = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.allClusterInput.Name = "allClusterInput";
            this.allClusterInput.Size = new System.Drawing.Size(85, 23);
            this.allClusterInput.TabIndex = 1;
            this.allClusterInput.ValueChanged += new System.EventHandler(this.allClusterInput_ValueChanged);
            // 
            // allClusterLabel
            // 
            this.allClusterLabel.AutoSize = true;
            this.allClusterLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allClusterLabel.Location = new System.Drawing.Point(6, 2);
            this.allClusterLabel.Name = "allClusterLabel";
            this.allClusterLabel.Size = new System.Drawing.Size(133, 15);
            this.allClusterLabel.TabIndex = 2;
            this.allClusterLabel.Text = "Höhe aller Cluster";
            // 
            // apiInfoButton
            // 
            this.apiInfoButton.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apiInfoButton.Location = new System.Drawing.Point(563, 0);
            this.apiInfoButton.Name = "apiInfoButton";
            this.apiInfoButton.Size = new System.Drawing.Size(75, 23);
            this.apiInfoButton.TabIndex = 3;
            this.apiInfoButton.Text = "API Info";
            this.apiInfoButton.UseVisualStyleBackColor = true;
            this.apiInfoButton.Click += new System.EventHandler(this.apiInfoButton_Click);
            // 
            // GeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.apiInfoButton);
            this.Controls.Add(this.allClusterLabel);
            this.Controls.Add(this.allClusterInput);
            this.Controls.Add(this.allClusterSlider);
            this.Name = "GeneralControl";
            this.Size = new System.Drawing.Size(662, 34);
            ((System.ComponentModel.ISupportInitialize)(this.allClusterSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allClusterInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar allClusterSlider;
        private System.Windows.Forms.NumericUpDown allClusterInput;
        private System.Windows.Forms.Label allClusterLabel;
        private System.Windows.Forms.Button apiInfoButton;
    }
}
