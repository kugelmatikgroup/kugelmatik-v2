﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KugelmatikLibrary;

namespace KugelmatikControl
{
    public partial class GeneralControl : UserControl
    {
        private Kugelmatik kugelmatik;
        private MainForm mainForm;

        public GeneralControl(Kugelmatik kugelmatik, MainForm mainForm)
        {
            this.kugelmatik = kugelmatik;
            this.mainForm = mainForm;
            InitializeComponent();

            Setup();
        }

        private void Setup()
        {
            allClusterSlider.Minimum = 0;
            allClusterSlider.Maximum = kugelmatik.ClusterConfig.MaxSteps;

            allClusterInput.Minimum = 0;
            allClusterInput.Maximum = kugelmatik.ClusterConfig.MaxSteps;
        }

        private void allClusterSlider_ValueChanged(object sender, EventArgs e)
        {
            allClusterInput.Value = allClusterSlider.Value;
            kugelmatik.SetAllClusters((ushort)allClusterInput.Value);
        }

        private void allClusterInput_ValueChanged(object sender, EventArgs e)
        {
            allClusterSlider.Value = (int)allClusterInput.Value;
        }

        private void apiInfoButton_Click(object sender, EventArgs e)
        {
            mainForm.onApiInfo_Click();
        }
    }
}
