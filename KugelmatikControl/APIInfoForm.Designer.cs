﻿
namespace KugelmatikControl
{
    partial class APIInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statsGroupBox = new System.Windows.Forms.GroupBox();
            this.receivedPacketsMinCount = new System.Windows.Forms.Label();
            this.receivedPacketsMinLabel = new System.Windows.Forms.Label();
            this.statsResetButton = new System.Windows.Forms.Button();
            this.failedPacketsCount = new System.Windows.Forms.Label();
            this.successfulPacketsCount = new System.Windows.Forms.Label();
            this.receivedPacketsCount = new System.Windows.Forms.Label();
            this.connectedClientsCount = new System.Windows.Forms.Label();
            this.failedPacketsLabel = new System.Windows.Forms.Label();
            this.successfulPacketsLabel = new System.Windows.Forms.Label();
            this.receivedPacketsLabel = new System.Windows.Forms.Label();
            this.connectedClientsLabel = new System.Windows.Forms.Label();
            this.statsChart = new LiveCharts.WinForms.CartesianChart();
            this.onlineServerGroupBox = new System.Windows.Forms.GroupBox();
            this.btServerOnlineLabel = new System.Windows.Forms.Label();
            this.ipServerOnlineLabel = new System.Windows.Forms.Label();
            this.btServerLabel = new System.Windows.Forms.Label();
            this.ipServerLabel = new System.Windows.Forms.Label();
            this.connectedClientsList = new System.Windows.Forms.ListBox();
            this.connectedClientsGroupBox = new System.Windows.Forms.GroupBox();
            this.b_removeClient = new System.Windows.Forms.Button();
            this.apiLogGroupBox = new System.Windows.Forms.GroupBox();
            this.apiLogClearButton = new System.Windows.Forms.Button();
            this.apiLogTextBox = new System.Windows.Forms.TextBox();
            this.b_removeAllClients = new System.Windows.Forms.Button();
            this.statsGroupBox.SuspendLayout();
            this.onlineServerGroupBox.SuspendLayout();
            this.connectedClientsGroupBox.SuspendLayout();
            this.apiLogGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // statsGroupBox
            // 
            this.statsGroupBox.Controls.Add(this.receivedPacketsMinCount);
            this.statsGroupBox.Controls.Add(this.receivedPacketsMinLabel);
            this.statsGroupBox.Controls.Add(this.statsResetButton);
            this.statsGroupBox.Controls.Add(this.failedPacketsCount);
            this.statsGroupBox.Controls.Add(this.successfulPacketsCount);
            this.statsGroupBox.Controls.Add(this.receivedPacketsCount);
            this.statsGroupBox.Controls.Add(this.connectedClientsCount);
            this.statsGroupBox.Controls.Add(this.failedPacketsLabel);
            this.statsGroupBox.Controls.Add(this.successfulPacketsLabel);
            this.statsGroupBox.Controls.Add(this.receivedPacketsLabel);
            this.statsGroupBox.Controls.Add(this.connectedClientsLabel);
            this.statsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.statsGroupBox.Name = "statsGroupBox";
            this.statsGroupBox.Size = new System.Drawing.Size(252, 161);
            this.statsGroupBox.TabIndex = 0;
            this.statsGroupBox.TabStop = false;
            this.statsGroupBox.Text = "API Stats";
            // 
            // receivedPacketsMinCount
            // 
            this.receivedPacketsMinCount.AutoSize = true;
            this.receivedPacketsMinCount.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivedPacketsMinCount.Location = new System.Drawing.Point(163, 107);
            this.receivedPacketsMinCount.Name = "receivedPacketsMinCount";
            this.receivedPacketsMinCount.Size = new System.Drawing.Size(13, 13);
            this.receivedPacketsMinCount.TabIndex = 10;
            this.receivedPacketsMinCount.Text = "0";
            // 
            // receivedPacketsMinLabel
            // 
            this.receivedPacketsMinLabel.AutoSize = true;
            this.receivedPacketsMinLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivedPacketsMinLabel.Location = new System.Drawing.Point(6, 107);
            this.receivedPacketsMinLabel.Name = "receivedPacketsMinLabel";
            this.receivedPacketsMinLabel.Size = new System.Drawing.Size(151, 13);
            this.receivedPacketsMinLabel.TabIndex = 9;
            this.receivedPacketsMinLabel.Text = "Empfangene Pakete / Min:";
            // 
            // statsResetButton
            // 
            this.statsResetButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statsResetButton.Location = new System.Drawing.Point(171, 132);
            this.statsResetButton.Name = "statsResetButton";
            this.statsResetButton.Size = new System.Drawing.Size(75, 23);
            this.statsResetButton.TabIndex = 8;
            this.statsResetButton.Text = "Reset";
            this.statsResetButton.UseVisualStyleBackColor = true;
            this.statsResetButton.Click += new System.EventHandler(this.statsResetButton_Click);
            // 
            // failedPacketsCount
            // 
            this.failedPacketsCount.AutoSize = true;
            this.failedPacketsCount.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.failedPacketsCount.Location = new System.Drawing.Point(163, 85);
            this.failedPacketsCount.Name = "failedPacketsCount";
            this.failedPacketsCount.Size = new System.Drawing.Size(13, 13);
            this.failedPacketsCount.TabIndex = 7;
            this.failedPacketsCount.Text = "0";
            // 
            // successfulPacketsCount
            // 
            this.successfulPacketsCount.AutoSize = true;
            this.successfulPacketsCount.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successfulPacketsCount.Location = new System.Drawing.Point(163, 62);
            this.successfulPacketsCount.Name = "successfulPacketsCount";
            this.successfulPacketsCount.Size = new System.Drawing.Size(13, 13);
            this.successfulPacketsCount.TabIndex = 6;
            this.successfulPacketsCount.Text = "0";
            // 
            // receivedPacketsCount
            // 
            this.receivedPacketsCount.AutoSize = true;
            this.receivedPacketsCount.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivedPacketsCount.Location = new System.Drawing.Point(163, 38);
            this.receivedPacketsCount.Name = "receivedPacketsCount";
            this.receivedPacketsCount.Size = new System.Drawing.Size(13, 13);
            this.receivedPacketsCount.TabIndex = 5;
            this.receivedPacketsCount.Text = "0";
            // 
            // connectedClientsCount
            // 
            this.connectedClientsCount.AutoSize = true;
            this.connectedClientsCount.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedClientsCount.Location = new System.Drawing.Point(163, 16);
            this.connectedClientsCount.Name = "connectedClientsCount";
            this.connectedClientsCount.Size = new System.Drawing.Size(13, 13);
            this.connectedClientsCount.TabIndex = 4;
            this.connectedClientsCount.Text = "0";
            // 
            // failedPacketsLabel
            // 
            this.failedPacketsLabel.AutoSize = true;
            this.failedPacketsLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.failedPacketsLabel.Location = new System.Drawing.Point(6, 85);
            this.failedPacketsLabel.Name = "failedPacketsLabel";
            this.failedPacketsLabel.Size = new System.Drawing.Size(151, 13);
            this.failedPacketsLabel.TabIndex = 3;
            this.failedPacketsLabel.Text = "Fehlgeschlagene Pakete: ";
            // 
            // successfulPacketsLabel
            // 
            this.successfulPacketsLabel.AutoSize = true;
            this.successfulPacketsLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successfulPacketsLabel.Location = new System.Drawing.Point(6, 62);
            this.successfulPacketsLabel.Name = "successfulPacketsLabel";
            this.successfulPacketsLabel.Size = new System.Drawing.Size(133, 13);
            this.successfulPacketsLabel.TabIndex = 2;
            this.successfulPacketsLabel.Text = "Erfolgreiche Pakete: ";
            // 
            // receivedPacketsLabel
            // 
            this.receivedPacketsLabel.AutoSize = true;
            this.receivedPacketsLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivedPacketsLabel.Location = new System.Drawing.Point(6, 38);
            this.receivedPacketsLabel.Name = "receivedPacketsLabel";
            this.receivedPacketsLabel.Size = new System.Drawing.Size(121, 13);
            this.receivedPacketsLabel.TabIndex = 1;
            this.receivedPacketsLabel.Text = "Empfangene Pakete: ";
            // 
            // connectedClientsLabel
            // 
            this.connectedClientsLabel.AutoSize = true;
            this.connectedClientsLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedClientsLabel.Location = new System.Drawing.Point(6, 16);
            this.connectedClientsLabel.Name = "connectedClientsLabel";
            this.connectedClientsLabel.Size = new System.Drawing.Size(127, 13);
            this.connectedClientsLabel.TabIndex = 0;
            this.connectedClientsLabel.Text = "Verbundene Clients: ";
            // 
            // statsChart
            // 
            this.statsChart.Location = new System.Drawing.Point(270, 12);
            this.statsChart.Name = "statsChart";
            this.statsChart.Size = new System.Drawing.Size(464, 236);
            this.statsChart.TabIndex = 1;
            this.statsChart.Text = "API Stats";
            // 
            // onlineServerGroupBox
            // 
            this.onlineServerGroupBox.Controls.Add(this.btServerOnlineLabel);
            this.onlineServerGroupBox.Controls.Add(this.ipServerOnlineLabel);
            this.onlineServerGroupBox.Controls.Add(this.btServerLabel);
            this.onlineServerGroupBox.Controls.Add(this.ipServerLabel);
            this.onlineServerGroupBox.Location = new System.Drawing.Point(12, 184);
            this.onlineServerGroupBox.Name = "onlineServerGroupBox";
            this.onlineServerGroupBox.Size = new System.Drawing.Size(252, 64);
            this.onlineServerGroupBox.TabIndex = 3;
            this.onlineServerGroupBox.TabStop = false;
            this.onlineServerGroupBox.Text = "Online Server";
            // 
            // btServerOnlineLabel
            // 
            this.btServerOnlineLabel.AutoSize = true;
            this.btServerOnlineLabel.Location = new System.Drawing.Point(69, 29);
            this.btServerOnlineLabel.Name = "btServerOnlineLabel";
            this.btServerOnlineLabel.Size = new System.Drawing.Size(19, 13);
            this.btServerOnlineLabel.TabIndex = 7;
            this.btServerOnlineLabel.Text = "❌";
            // 
            // ipServerOnlineLabel
            // 
            this.ipServerOnlineLabel.AutoSize = true;
            this.ipServerOnlineLabel.Location = new System.Drawing.Point(69, 16);
            this.ipServerOnlineLabel.Name = "ipServerOnlineLabel";
            this.ipServerOnlineLabel.Size = new System.Drawing.Size(19, 13);
            this.ipServerOnlineLabel.TabIndex = 6;
            this.ipServerOnlineLabel.Text = "❌";
            // 
            // btServerLabel
            // 
            this.btServerLabel.AutoSize = true;
            this.btServerLabel.Location = new System.Drawing.Point(6, 29);
            this.btServerLabel.Name = "btServerLabel";
            this.btServerLabel.Size = new System.Drawing.Size(61, 13);
            this.btServerLabel.TabIndex = 5;
            this.btServerLabel.Text = "BT Server: ";
            // 
            // ipServerLabel
            // 
            this.ipServerLabel.AutoSize = true;
            this.ipServerLabel.Location = new System.Drawing.Point(6, 16);
            this.ipServerLabel.Name = "ipServerLabel";
            this.ipServerLabel.Size = new System.Drawing.Size(57, 13);
            this.ipServerLabel.TabIndex = 4;
            this.ipServerLabel.Text = "IP Server: ";
            // 
            // connectedClientsList
            // 
            this.connectedClientsList.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedClientsList.FormattingEnabled = true;
            this.connectedClientsList.Location = new System.Drawing.Point(9, 19);
            this.connectedClientsList.Name = "connectedClientsList";
            this.connectedClientsList.Size = new System.Drawing.Size(252, 134);
            this.connectedClientsList.TabIndex = 5;
            this.connectedClientsList.SelectedIndexChanged += new System.EventHandler(this.connectedClientsList_SelectedIndexChanged);
            this.connectedClientsList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.connectedClientsList_KeyDown);
            // 
            // connectedClientsGroupBox
            // 
            this.connectedClientsGroupBox.Controls.Add(this.b_removeAllClients);
            this.connectedClientsGroupBox.Controls.Add(this.b_removeClient);
            this.connectedClientsGroupBox.Controls.Add(this.connectedClientsList);
            this.connectedClientsGroupBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedClientsGroupBox.Location = new System.Drawing.Point(12, 265);
            this.connectedClientsGroupBox.Name = "connectedClientsGroupBox";
            this.connectedClientsGroupBox.Size = new System.Drawing.Size(296, 203);
            this.connectedClientsGroupBox.TabIndex = 7;
            this.connectedClientsGroupBox.TabStop = false;
            this.connectedClientsGroupBox.Text = "Connected clients";
            // 
            // b_removeClient
            // 
            this.b_removeClient.Enabled = false;
            this.b_removeClient.ForeColor = System.Drawing.Color.DarkRed;
            this.b_removeClient.Location = new System.Drawing.Point(267, 19);
            this.b_removeClient.Name = "b_removeClient";
            this.b_removeClient.Size = new System.Drawing.Size(20, 23);
            this.b_removeClient.TabIndex = 6;
            this.b_removeClient.Text = "✖";
            this.b_removeClient.UseVisualStyleBackColor = true;
            this.b_removeClient.Click += new System.EventHandler(this.removeClientButton_Click);
            // 
            // apiLogGroupBox
            // 
            this.apiLogGroupBox.Controls.Add(this.apiLogClearButton);
            this.apiLogGroupBox.Controls.Add(this.apiLogTextBox);
            this.apiLogGroupBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apiLogGroupBox.Location = new System.Drawing.Point(314, 265);
            this.apiLogGroupBox.Name = "apiLogGroupBox";
            this.apiLogGroupBox.Size = new System.Drawing.Size(420, 203);
            this.apiLogGroupBox.TabIndex = 8;
            this.apiLogGroupBox.TabStop = false;
            this.apiLogGroupBox.Text = "API Log";
            // 
            // apiLogClearButton
            // 
            this.apiLogClearButton.Location = new System.Drawing.Point(345, 180);
            this.apiLogClearButton.Name = "apiLogClearButton";
            this.apiLogClearButton.Size = new System.Drawing.Size(75, 23);
            this.apiLogClearButton.TabIndex = 1;
            this.apiLogClearButton.Text = "Clear";
            this.apiLogClearButton.UseVisualStyleBackColor = true;
            this.apiLogClearButton.Click += new System.EventHandler(this.apiLogClearButton_Click);
            // 
            // apiLogTextBox
            // 
            this.apiLogTextBox.Location = new System.Drawing.Point(6, 19);
            this.apiLogTextBox.Multiline = true;
            this.apiLogTextBox.Name = "apiLogTextBox";
            this.apiLogTextBox.ReadOnly = true;
            this.apiLogTextBox.Size = new System.Drawing.Size(408, 155);
            this.apiLogTextBox.TabIndex = 0;
            // 
            // b_removeAllClients
            // 
            this.b_removeAllClients.ForeColor = System.Drawing.Color.Black;
            this.b_removeAllClients.Location = new System.Drawing.Point(9, 159);
            this.b_removeAllClients.Name = "b_removeAllClients";
            this.b_removeAllClients.Size = new System.Drawing.Size(105, 23);
            this.b_removeAllClients.TabIndex = 7;
            this.b_removeAllClients.Text = "Disconnect all";
            this.b_removeAllClients.UseVisualStyleBackColor = true;
            this.b_removeAllClients.Click += new System.EventHandler(this.b_removeAllClients_Click);
            // 
            // APIInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 475);
            this.Controls.Add(this.apiLogGroupBox);
            this.Controls.Add(this.connectedClientsGroupBox);
            this.Controls.Add(this.statsChart);
            this.Controls.Add(this.onlineServerGroupBox);
            this.Controls.Add(this.statsGroupBox);
            this.Name = "APIInfoForm";
            this.Text = "API Info";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.APIInfoForm_FormClosing);
            this.statsGroupBox.ResumeLayout(false);
            this.statsGroupBox.PerformLayout();
            this.onlineServerGroupBox.ResumeLayout(false);
            this.onlineServerGroupBox.PerformLayout();
            this.connectedClientsGroupBox.ResumeLayout(false);
            this.apiLogGroupBox.ResumeLayout(false);
            this.apiLogGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox statsGroupBox;
        private System.Windows.Forms.Label failedPacketsLabel;
        private System.Windows.Forms.Label successfulPacketsLabel;
        private System.Windows.Forms.Label receivedPacketsLabel;
        private System.Windows.Forms.Label connectedClientsLabel;
        private System.Windows.Forms.Label connectedClientsCount;
        private System.Windows.Forms.Label receivedPacketsCount;
        private System.Windows.Forms.Label failedPacketsCount;
        private System.Windows.Forms.Label successfulPacketsCount;
        private System.Windows.Forms.Button statsResetButton;
        private LiveCharts.WinForms.CartesianChart statsChart;
        private System.Windows.Forms.Label receivedPacketsMinLabel;
        private System.Windows.Forms.Label receivedPacketsMinCount;
        private System.Windows.Forms.GroupBox onlineServerGroupBox;
        private System.Windows.Forms.Label btServerOnlineLabel;
        private System.Windows.Forms.Label ipServerOnlineLabel;
        private System.Windows.Forms.Label btServerLabel;
        private System.Windows.Forms.Label ipServerLabel;
        private System.Windows.Forms.ListBox connectedClientsList;
        private System.Windows.Forms.GroupBox connectedClientsGroupBox;
        private System.Windows.Forms.Button b_removeClient;
        private System.Windows.Forms.GroupBox apiLogGroupBox;
        private System.Windows.Forms.TextBox apiLogTextBox;
        private System.Windows.Forms.Button apiLogClearButton;
        private System.Windows.Forms.Button b_removeAllClients;
    }
}